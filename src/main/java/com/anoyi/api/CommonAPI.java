package com.anoyi.api;

import com.anoyi.bean.Comment;
import com.anoyi.bean.MessageBean;
import com.anoyi.bean.ResponseBean;
import com.anoyi.mongo.service.CommentService;
import com.anoyi.tools.DingTalkTools;
import lombok.AllArgsConstructor;
import lombok.extern.log4j.Log4j2;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * 通用 API
 */
@RestController
@AllArgsConstructor
@RequestMapping("/api/common")
@Log4j2
public class CommonAPI {

	private final CommentService commentService;

    /**
     * 用户留言
     */
    @PostMapping(value = "/message")
    public ResponseBean message(MessageBean messageBean) {
        DingTalkTools.textMessage(messageBean);
        return ResponseBean.success(null);
    }

    /**
     * 文章评论
     */
    @PostMapping(value = "/comment")
    public ResponseBean message(Comment comment) {
        comment.setShow(false);
        commentService.save(comment);
        MessageBean messageBean = new MessageBean();
        messageBean.setContent(comment.getContent());
        messageBean.setEmail(comment.getEmail());
        messageBean.setNickname(comment.getName());
        DingTalkTools.textMessage(messageBean);
        return ResponseBean.success(null);
    }

}
